package eventos;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class PruebaEventosVentana2 {
	
	public static void main(String[] args){
		
		MarcoConEventos4 miMarco1 = new MarcoConEventos4();
		miMarco1.setTitle("Marco 1");
		miMarco1.setBounds(300,300,500,350);
		
		MarcoConEventos4 miMarco2 = new MarcoConEventos4();
		miMarco2.setTitle("Marco 2");
		miMarco2.setBounds(900,300,500,350);
		
	}

}

class MarcoConEventos4 extends JFrame{
	
	public MarcoConEventos4(){
		
		setVisible(true);
		LaminaConEventos4 miLamina = new LaminaConEventos4();
		addWindowListener(miLamina);
		
	}
	
}

class LaminaConEventos4 extends WindowAdapter{
	
	public void windowIconified(WindowEvent event) {
		System.out.println("Ventana minimizada.");
	}
	
}