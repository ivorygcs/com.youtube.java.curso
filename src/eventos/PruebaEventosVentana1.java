package eventos;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class PruebaEventosVentana1 {

	public static void main(String[] args){
		
		MarcoConEventos3 miMarco = new MarcoConEventos3();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		miMarco.setTitle("Ventana 1");
		miMarco.setBounds(300,300,500,350);
		
		MarcoConEventos3 miMarco2 = new MarcoConEventos3();
		miMarco2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		miMarco2.setTitle("Ventana 2");
		miMarco2.setBounds(900,300,500,350);
	}
	
}

class MarcoConEventos3 extends JFrame{
	
	public MarcoConEventos3(){
		
		//setTitle("Curso swing: Marco con eventos 3.");
		//setBounds(300,300,500,350);
		setVisible(true);
		
		LaminaConEventos3 miLamina = new LaminaConEventos3();
		addWindowListener(miLamina);
		
	}
	
}

class LaminaConEventos3 implements WindowListener{

	@Override
	public void windowActivated(WindowEvent arg0) {
		System.out.println("Ventana activada.");
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		System.out.println("Ventana cerrada.");
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		System.out.println("Cerrando ventana.");
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		System.out.println("Ventana desactivada.");
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		System.out.println("Ventana restaurada.");
	}

	@Override
	public void windowIconified(WindowEvent event) {
		System.out.println("Ventana minimizada.");
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		System.out.println("Ventana abierta.");
	}
	
}