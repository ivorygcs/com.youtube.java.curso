package eventos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaEventosBoton1 {

	public static void main(String[] args){
		
		MarcoConEvento1 miMarco = new MarcoConEvento1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConEvento1 extends JFrame{
	
	public MarcoConEvento1(){
		
		setTitle("Aprendiendo swing: Marco con evento.");
		setBounds(700,300,500,300);
		
		LaminaConEvento1 miLamina = new LaminaConEvento1();
		add(miLamina);
		
	}
	
}

class LaminaConEvento1 extends JPanel implements ActionListener{
	
	
	JButton botonAzul = new JButton("Azul");
	JButton botonAmarillo = new JButton("Amarillo");
	JButton botonRojo = new JButton("Rojo");
	
	
	public LaminaConEvento1(){
		
		add(botonAzul);
		botonAzul.addActionListener(this);
		
		add(botonAmarillo);
		botonAmarillo.addActionListener(this);
		
		add(botonRojo);
		botonRojo.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		
		Object botonPulsado = event.getSource();
		
		if(botonPulsado.equals(botonAzul)){
			setBackground(Color.BLUE);			
		}else if(botonPulsado.equals(botonAmarillo)){
			setBackground(Color.YELLOW);
		}else{
			setBackground(Color.RED);
		}
		
	}
	
}



