package eventos;

import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;

public class PruebaEventosVentana3 {

	public static void main(String[] args){
		
		MarcoConEventos5 miMarco = new MarcoConEventos5();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConEventos5 extends JFrame{
	
	public MarcoConEventos5(){
		setTitle("Aprendiendo swing: Marco con eventos 5");
		setVisible(true);
		setBounds(300, 300, 500, 350);
		
		CambioEstado cambioEstado = new CambioEstado();
		addWindowStateListener(cambioEstado);
	}
	
}

class CambioEstado implements WindowStateListener{

	@Override
	public void windowStateChanged(WindowEvent event) {		
		if(event.getOldState()==JFrame.NORMAL){
			System.out.println("La pantalla estaba en estado normal.");
		}
	}
	
}