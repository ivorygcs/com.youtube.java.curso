package eventos;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class PruebasEventosRaton1 {
	
	public static void main(String[] args){
		
		MarcoConEventosRaton1 miMarco = new MarcoConEventosRaton1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoConEventosRaton1 extends JFrame{
	
	public MarcoConEventosRaton1(){
		
		setTitle("Aprendiendo swing: Eventos Raton 1");
		setBounds(700,300,600,450);
		
		EventosRaton eventosListener = new EventosRaton();
		addMouseListener(eventosListener);
		
	}
	
}

class EventosRaton implements MouseListener{

	@Override
	public void mouseClicked(MouseEvent arg0) {
		System.out.println("Has echo click !!");
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		System.err.println("Acabas de entrar !");
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		System.err.println("Acabas de salir !");
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		System.out.println("Boton pulsado !");
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		System.out.println("Boton soltado !");
	}
	
}