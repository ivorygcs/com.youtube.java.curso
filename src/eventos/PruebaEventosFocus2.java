package eventos;

import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import javax.swing.JFrame;

public class PruebaEventosFocus2 extends JFrame implements WindowFocusListener  {
	
	PruebaEventosFocus2 marco1, marco2;
	
	public static void main(String[] args){
	
		PruebaEventosFocus2 miApp = new PruebaEventosFocus2();
		miApp.crearMarco();
		
	}
	
	public void crearMarco(){
		
		marco1 = new PruebaEventosFocus2();
		marco1.setVisible(true);
		marco1.setBounds(300, 100, 600, 350);
		marco1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		marco1.addWindowFocusListener(this);
		
		marco2 = new PruebaEventosFocus2();
		marco2.setVisible(true);
		marco2.setBounds(1200,100,600,350);
		marco2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		marco2.addWindowFocusListener(this);
		
	}
	
	@Override
	public void windowGainedFocus(WindowEvent event) {
		
		if(event.getSource()==marco1){
			marco1.setTitle("Tengo el foco");
		}else if(event.getSource()==marco2){
			marco2.setTitle("Tengo el foco");
		}
		
	}

	@Override
	public void windowLostFocus(WindowEvent event) {
		
		if(event.getSource()==marco1){
			marco1.setTitle("");
		}else if(event.getSource()==marco2){
			marco2.setTitle("");
		}
		
	}
	
}
