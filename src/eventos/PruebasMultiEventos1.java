package eventos;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebasMultiEventos1 {
	
	public static void main(String[] args){
	
		MarcoMultiEventos1 miMarco = new MarcoMultiEventos1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoMultiEventos1 extends JFrame{
	
	public MarcoMultiEventos1(){
		
		setTitle("Marco multi eventos 1");
		setBounds(600,350,600,300);
		
		add(new LaminaMultiEventos1());
		
	}
	
}

class LaminaMultiEventos1 extends JPanel{
	
	public LaminaMultiEventos1(){
		
		JButton botonAmarillo = new JButton("Amarillo");
		add(botonAmarillo);
		
		JButton botonRojo = new JButton("Rojo");
		add(botonRojo);
		
		JButton botonVerde = new JButton("Verde");
		add(botonVerde);
		
	}
	
}

class AccionColor extends AbstractAction{	
	
	
	public AccionColor(String nombre, Icon icono, Color colorBoton){
		
		putValue(Action.NAME, nombre);
		
		
	}
	
	public void actionPerformed(ActionEvent event) {
		
	}
	
}




