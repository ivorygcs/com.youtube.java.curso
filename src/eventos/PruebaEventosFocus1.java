package eventos;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PruebaEventosFocus1 {

	public static void main(String[] args){
		
		MarcoConEventosFocus1 miMarco = new MarcoConEventosFocus1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConEventosFocus1 extends JFrame{
	
	public MarcoConEventosFocus1(){
		
		setTitle("Marco con eventos focus 1.");
		setBounds(700,300,600,450);
		
		LaminaConEventosFocus1 miLamina = new LaminaConEventosFocus1();
		add(miLamina);
		
	}
	
}

class LaminaConEventosFocus1 extends JPanel{
	
	JTextField cuadroTexto1, cuadroTexto2;
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		
		setLayout(null);
		
		cuadroTexto1 = new JTextField();
		cuadroTexto1.setBounds(120, 10, 150, 20);
		add(cuadroTexto1);
		
		cuadroTexto2 = new JTextField();
		cuadroTexto2.setBounds(120, 50, 150, 20);
		add(cuadroTexto2);	
		
		EventosFocos eventosFocos = new EventosFocos();
		cuadroTexto1.addFocusListener(eventosFocos);
		
	}
	
	private class EventosFocos implements FocusListener{
		
		@Override
		public void focusGained(FocusEvent arg0) {
			
		}
		
		@Override
		public void focusLost(FocusEvent event) {
			
			String dirEmail = cuadroTexto1.getText();
			boolean checkMail = false;
			
			for(int posChar = 0; posChar < dirEmail.length(); posChar++){
				if(dirEmail.charAt(posChar)=='@'){
					checkMail = true;
				}
			}
			
			if(checkMail){
				System.out.println("CORRECTO");
			}else{
				System.err.println("INCORRECTO");
			}
			
		}
		
	}

}




