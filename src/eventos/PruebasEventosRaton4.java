package eventos;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;

public class PruebasEventosRaton4 {

	public static void main(String[] args){
		
		MarcoConEventosRaton4 miMarco = new MarcoConEventosRaton4();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConEventosRaton4 extends JFrame{
	
	public MarcoConEventosRaton4(){
		
		setTitle("");
		setBounds(700,300,600,450);
		
		EventosRaton4 eventosRaton4 = new EventosRaton4();
		addMouseMotionListener(eventosRaton4);
	
	}
	
}

class EventosRaton4 implements MouseMotionListener{

	@Override
	public void mouseDragged(MouseEvent arg0) {
		
		System.out.println("Arrastrando ...");
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
		System.out.println("Moviendo ...");
		
	}
	
}