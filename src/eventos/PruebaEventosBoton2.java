package eventos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.sun.xml.internal.ws.api.pipe.ServerTubeAssemblerContext;

public class PruebaEventosBoton2 {

	public static void main(String[] args){
		
		MarcoConEventos2 miMarco = new MarcoConEventos2();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}
	
}

class MarcoConEventos2 extends JFrame{
	
	public MarcoConEventos2(){
		
		setTitle("Curso swing: Marco con eventos 2.");
		setBounds(700,300,500,300);
		
		LaminaConEventos2 miLamina = new LaminaConEventos2();
		add(miLamina);
	}
	
}

class LaminaConEventos2 extends JPanel{
	
	JButton botonAzul = new JButton("Azul");
	JButton botonAmarillo = new JButton("Amarillo");
	JButton botonRojo = new JButton("Rojo");
	
	public LaminaConEventos2(){
		
		add(botonAzul);
		botonAzul.addActionListener(new ColorFondo(Color.BLUE));
		
		add(botonAmarillo);
		botonAmarillo.addActionListener(new ColorFondo(Color.YELLOW));
		
		add(botonRojo);
		botonRojo.addActionListener(new ColorFondo(Color.RED));
		
	}
	
	class ColorFondo implements ActionListener{
		
		private Color colorDeFondo;
		
		public ColorFondo(Color color){
			colorDeFondo = color;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			setBackground(colorDeFondo);
			
		}
		
	}
	
}




