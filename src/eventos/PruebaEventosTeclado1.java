package eventos;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class PruebaEventosTeclado1 {
	
	public static void main(String[] args){
		
		MarcoEventosTeclado1 miMarco = new MarcoEventosTeclado1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoEventosTeclado1 extends JFrame{
	
	public MarcoEventosTeclado1(){
		
		setTitle("Marco eventos teclado 1.");
		setBounds(700, 300, 600, 450);
		
		EventoTeclado eventoTeclado = new EventoTeclado();
		addKeyListener(eventoTeclado);
		
	}
	
}

class EventoTeclado implements KeyListener{

	@Override
	public void keyPressed(KeyEvent event) {
		
		//int keyCode = event.getKeyCode();
		//System.out.println(keyCode);
		
		
	}

	@Override
	public void keyReleased(KeyEvent event) {
		
	}

	@Override
	public void keyTyped(KeyEvent event) {
		
		char keyTyped = event.getKeyChar();
		System.out.println(keyTyped);
		
	}
	
}