package eventos;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

public class PruebasEventosRaton2 {

	public static void main(String[] args){
		
		MarcoConEventosRaton2 miMarco = new MarcoConEventosRaton2();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConEventosRaton2 extends JFrame{
	
	public MarcoConEventosRaton2(){
		
		setTitle("Aprendiendo swing: Marco con eventos Raton 2");
		setBounds(700,300,600,450);
		
		EventosRaton2 eventoMouse2 = new EventosRaton2();
		addMouseListener(eventoMouse2);
		
	}
	
}

class EventosRaton2 extends MouseAdapter{
	
	public void mouseClicked(MouseEvent event){
		System.out.println("Has echo click !");
	}
	
}