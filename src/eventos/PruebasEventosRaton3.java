package eventos;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

public class PruebasEventosRaton3 {

	public static void main(String[] args){
		
		MarcoConEventosRaton3 miMarco = new MarcoConEventosRaton3();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConEventosRaton3 extends JFrame{
	
	public MarcoConEventosRaton3(){
		
		setTitle("Marco con eventos raton 3");
		setBounds(700,300,600,450);
		
		EventoRaton3 eventoRaton3 = new EventoRaton3();
		addMouseListener(eventoRaton3);
		
	}
}

class EventoRaton3 extends MouseAdapter{
	
	/*public void mouseClicked(MouseEvent event){
		//System.out.println("[X]: "+event.getX()+" [Y]: "+event.getY());
		System.out.println(event.getClickCount());
	
	}*/
	
	public void mousePressed(MouseEvent event){
		
		if(event.getModifiersEx()==MouseEvent.BUTTON1_DOWN_MASK){
			
			System.out.println("Boton IZQUIERDO !!");
		
		}else if(event.getModifiersEx()==MouseEvent.BUTTON3_DOWN_MASK){
			
			System.out.println("Boton DERECHO !!");
		
		}
		
	}
	
}


