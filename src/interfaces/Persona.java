package interfaces;

public abstract class Persona {
	
	// Atributos
	private String sNombre;
	private String sApellidos;
	private String sDNI;

	// Constructor por defecto
	public Persona(){
		
	}
	
	// Constructor con parametros
	public Persona(String sNombre, String sApellidos, int iNumeroDNI){
		this.sNombre = sNombre;
		this.sApellidos = sApellidos;
		this.sDNI = iNumeroDNI+"-"+calculaLetraDNI(iNumeroDNI);
	}
	
	// Metodo para calcular la letra del DNI
	public char calculaLetraDNI(int iNumeroDNI){
		
		String sLetrasDNI = "TRWAGMYFPDXBNJZSQVHLCKE";
		
		int iPosLetra = iNumeroDNI % 23;
		char cLetra = sLetrasDNI.charAt(iPosLetra);
		
		return cLetra;
		
	}
	
	// Getters && Setters
	public String getsNombre() {
		return sNombre;
	}

	public void setsNombre(String sNombre) {
		this.sNombre = sNombre;
	}

	public String getsApellidos() {
		return sApellidos;
	}

	public void setsApellidos(String sApellidos) {
		this.sApellidos = sApellidos;
	}

	public String getsDNI() {
		return sDNI;
	}

	public void setsDNI(int iNumeroDNI) {
		this.sDNI = iNumeroDNI+"-"+calculaLetraDNI(iNumeroDNI);
	}
	
	// Sobrescribimos el metodo toString
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" [Nombre] ");
		builder.append(sNombre);
		builder.append(" [Apellidos] ");
		builder.append(sApellidos);
		builder.append(" [DNI] ");
		builder.append(sDNI);
		return builder.toString();
	}
	
	
}
