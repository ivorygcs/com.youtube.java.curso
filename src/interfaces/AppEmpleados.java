package interfaces;

import java.util.Arrays;

public class AppEmpleados {

	public static void main(String[] args) {
		
		int iTotEmp = 5;
		Empleado[] eListadoEmpleados = new Empleado[iTotEmp];
		
		eListadoEmpleados[0] = new Empleado("Guillem", "Casas Simon", 43631839, 975);
		eListadoEmpleados[1] = new Empleado("Marta", "Casal Llunell", 37734374, 1050);
		eListadoEmpleados[2] = new Empleado("Adria", "Casals Llunell", 12345678, 750);
		eListadoEmpleados[3] = new Empleado("Nuria", "Vila Sanchez", 4837425, 695);
		eListadoEmpleados[4] = new Empleado("Francis", "Paneque Perez", 4839127, 950);
		
		// Mostramos el array desordenado
		for(Empleado emp : eListadoEmpleados){
			System.out.println(emp);
		}
		
		Arrays.sort(eListadoEmpleados); // Ordenamos el array con el uso de la interfaz comparable 
		System.out.println(); // Forzamos el salto de linea
		
		// Mostramos el array desordenado
		for(Empleado emp : eListadoEmpleados){
			System.out.println(emp);
		}
		
		System.out.println();
		
		JefeProyecto jefe = new JefeProyecto("Guillem", "Casas Simon", 43631839, 1250);
		Empleado empleado = new Empleado("Marta", "Casals Llunell", 37734374, 1050);
		
		jefe.setdSueldo(jefe.estableceBonus(jefe.getdSueldo()+750));
		System.out.println(jefe);
		
		empleado.setdSueldo(empleado.estableceBonus(empleado.getdSueldo()+750));
		System.out.println(empleado);

	}

}
