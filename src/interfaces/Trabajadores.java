package interfaces;

public interface Trabajadores {
	
	// Constantes
	public static final double bonusBase = 1500;
	
	// Metodos
	public abstract double estableceBonus(double gratificacion);

}
