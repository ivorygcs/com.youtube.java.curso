package interfaces;

public class JefeProyecto extends Empleado implements Jefes {
	
	// Constructor
	public JefeProyecto(String sNombre, String sApellidos, int iNumeroDNI, double dSueldo) {
		super(sNombre, sApellidos, iNumeroDNI, dSueldo);
	}

	// Sobrescribimos el metodo tomaDecisiones de la interface Jefes
	@Override
	public String tomaDecisiones(String decision) {
		
		return "ha tomado la decision de: "+decision;
			
	}
	
	// Sobrescribimos el metodo estableceBonus de la interfaz Trabajadores
	@Override
	public double estableceBonus(double gratificacion) {
		
		double prima = 2000;
		return Trabajadores.bonusBase+gratificacion+prima;
	
	}	

}
