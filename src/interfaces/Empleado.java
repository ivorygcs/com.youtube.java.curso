package interfaces;

@SuppressWarnings("rawtypes")
public class Empleado extends Persona implements Comparable, Trabajadores{
	
	// Atributos
	private double dSueldo;
	
	// Constructor
	public Empleado(String sNombre, String sApellidos, int iNumeroDNI, double dSueldo){
		super(sNombre, sApellidos, iNumeroDNI);
		this.dSueldo = dSueldo;
	}
	
	// Getters y Setters
	public double getdSueldo() {
		return dSueldo;
	}

	public void setdSueldo(double dSueldo) {
		this.dSueldo = dSueldo;
	}

	// Sobreescribimos el metodo toString
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" [Nombre] ");
		builder.append(getsNombre());
		builder.append(" [Apellidos] ");
		builder.append(getsApellidos());
		builder.append(" [DNI] ");
		builder.append(getsDNI());
		builder.append(" [Sueldo] ");
		builder.append(dSueldo);
		return builder.toString();
	}
	
	// Sobrescribimos el metodo comparteTo de la interfaz Comparable
	@Override
	public int compareTo(Object objEmp) {
		
		Empleado objEmpleado = (Empleado) objEmp;
		
		if(this.dSueldo < objEmpleado.dSueldo){
			return -1;
		}else if(this.dSueldo > objEmpleado.dSueldo){
			return 1;
		}else{
			return 0;
		}
		
	}
	
	// Sobreescribimos el metodo estableceBonus de la interfaz Trabajadores
	@Override
	public double estableceBonus(double gratificacion) {
		
		return Trabajadores.bonusBase+gratificacion;
	
	}
	
}
