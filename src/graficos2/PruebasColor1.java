package graficos2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.SystemColor;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebasColor1 {
	
	public static void main(String[] args){
		
		MarcoConColor1 miMarco = new MarcoConColor1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoConColor1 extends JFrame{
	
	public MarcoConColor1(){
		
		setTitle("Aprendiendo swing: Marco con color 1");
		setSize(400,400);
	
		LaminaConColor1 miLamina = new LaminaConColor1();
		miLamina.setBackground(SystemColor.window);
		add(miLamina);
		
	}
	
}

class LaminaConColor1 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Rectangle2D rectangle2D = new Rectangle2D.Double(100,100,200,150);
		
		graphics2D.setPaint(Color.RED);
		graphics2D.fill(rectangle2D);
		
		Ellipse2D ellipse2D = new Ellipse2D.Double();
		ellipse2D.setFrame(rectangle2D);
		graphics2D.setPaint(new Color(109,172,59));
		graphics2D.fill(ellipse2D);
		
	}
	
}








