package graficos2;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.imageio.*;

public class PruebaImagen1 {

	public static void main(String[] args){
		
		MarcoConImagen1 miMarco = new MarcoConImagen1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConImagen1 extends JFrame{
	
	public MarcoConImagen1(){
		
		setTitle("Curso swing: Marco con imagen.");
		setSize(400,400);
		
		LaminaConImagen1 miLamina = new LaminaConImagen1();
		add(miLamina);
		
	}
	
}

class LaminaConImagen1 extends JPanel{
	
	private Image image;
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		
		File fileImage = new File("src/graficos2/bd.png");
		
		try {
			image = ImageIO.read(fileImage);
		} catch (IOException e) {
			System.out.println("La imagen no se encuentra.");
		}

		graphics.drawImage(image, 100, 100, null);
		
	}
	
}




