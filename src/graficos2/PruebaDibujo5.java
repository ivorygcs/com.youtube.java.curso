package graficos2;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaDibujo5 {

	public static void main(String[] args){
		
		MarcoConDibujo5 miMarco = new MarcoConDibujo5();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConDibujo5 extends JFrame{
	
	public MarcoConDibujo5(){
		
		setTitle("Aprendiendo swing: Marco con dibujo 5");
		setSize(400,400);
		
		LaminaConDibujo5 miLamina = new LaminaConDibujo5();
		add(miLamina);
		
	}
	
}

class LaminaConDibujo5 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Line2D line2D = new Line2D.Double(100,100,300,100);
		
		graphics2D.draw(line2D);
		
	}
	
}