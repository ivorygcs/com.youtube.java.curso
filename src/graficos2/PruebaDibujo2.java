package graficos2;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaDibujo2 {

	public static void main(String[] args){
		MarcoConDibujo2 miMarco = new MarcoConDibujo2();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}

class MarcoConDibujo2 extends JFrame{
	
	public MarcoConDibujo2(){
		
		setTitle("Aprendiendo swing: Marco con dibujo 2");
		setSize(400,400);
		
		LaminaConDibujo2 miLamina = new LaminaConDibujo2();
		add(miLamina);
		
	}
	
}

class LaminaConDibujo2 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Rectangle2D rectangle2D = new Rectangle2D.Double(100,100,200,150);
		
		graphics2D.draw(rectangle2D);
		
	}
	
}