package graficos2;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaDibujo {
	
	public static void main(String[] args){
		MarcoConDibujos miMarco = new MarcoConDibujos();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoConDibujos extends JFrame{
	
	public MarcoConDibujos(){
		
		setTitle("Prueba Dibujo");
		setSize(400,400);
		
		LaminaConDibujos miLamina = new LaminaConDibujos();
		add(miLamina);
	
	}
	
}

class LaminaConDibujos extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		
		//graphics.drawRect(50,50, 200, 200);
		//graphics.drawLine(50, 50, 250, 50);
		//graphics.drawArc(50, 100, 100, 200, 120, 150);
		
	}
	
}


