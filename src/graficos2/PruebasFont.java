package graficos2;

import java.awt.GraphicsEnvironment;
import javax.swing.*;

public class PruebasFont {

	public static void main(String[] args){
		
		String searchFont = JOptionPane.showInputDialog("Introduce la fuente a comprobar.");
		boolean checkFont = false;
		
		String[] nombreFuentes = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		
		for(String font : nombreFuentes){
			if(font.equals(searchFont)){
				checkFont = true;
			}
		}
		
		if(checkFont){
			System.out.println("Fuente "+searchFont+" instalada.");
		}else{
			System.out.println("Fuente "+searchFont+" NO instalada.");
		}
		
		
	}
	
}
