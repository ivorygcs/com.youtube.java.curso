package graficos2;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaFuentes1 {

	public static void main(String[] args){
		
		MarcoConFuentes1 miMarco = new MarcoConFuentes1();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConFuentes1 extends JFrame{
	
	public MarcoConFuentes1(){
		
		setTitle("Curso swing: Marco con fuentes 1");
		setSize(400,400);
		
		LaminaConFuentes1 miLamina = new LaminaConFuentes1();
		add(miLamina);
		
	}
	
}

class LaminaConFuentes1 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Font font = new Font("Arial",Font.BOLD,14);
		
		graphics2D.setFont(font);
		graphics2D.setColor(Color.BLUE);
		
		graphics2D.drawString("Guillem Casas", 100, 100);
		
		
	}
	
}

