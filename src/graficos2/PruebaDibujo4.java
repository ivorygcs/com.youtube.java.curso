package graficos2;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaDibujo4 {

	public static void main(String[] args){
		
		MarcoConDibujo4 miMarco = new MarcoConDibujo4();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
}

class MarcoConDibujo4 extends JFrame{
	
	public MarcoConDibujo4(){
	
		setTitle("Aprendiendo swing: Marco con dibujo 4.");
		setSize(400,400);
		
		LaminaConDibujo4 miLamina = new LaminaConDibujo4();
		add(miLamina);
		
	}
	
}

class LaminaConDibujo4 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Rectangle2D rectangle2D = new Rectangle2D.Double(100,100,250,125);
		
		Ellipse2D elipse2D = new Ellipse2D.Double();
		elipse2D.setFrame(rectangle2D);
		
		graphics2D.draw(rectangle2D);
		
	}
	
}