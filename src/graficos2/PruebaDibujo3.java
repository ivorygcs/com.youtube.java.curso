package graficos2;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaDibujo3 {

	public static void main(String[] args){

		MarcoConDibujo3 miMarco = new MarcoConDibujo3();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoConDibujo3 extends JFrame{
	
	public MarcoConDibujo3(){
		
		setTitle("Curso swing: Prueba de dibujo 3");
		setSize(400,400);
		
		LaminaConDibujo3 miLamina = new LaminaConDibujo3();
		add(miLamina);
	
	}
	
}

class LaminaConDibujo3 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
	
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Ellipse2D elipse2d = new Ellipse2D.Double(100,100,250,150);
		
		graphics2D.draw(elipse2d);
	
	}
	
}
