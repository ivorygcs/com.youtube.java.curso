package graficos;

import java.awt.GraphicsEnvironment;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class FuentesInstSys {
	
	public static void main(String[] args){
		
		String sConsFont = JOptionPane.showInputDialog("Introduce el tipo de letra a comprobar: ");
		Boolean bCheckFont = false;
		
		// Podemos comprobar todas las fuentes que hay instaladas en el sistema nativo.
		String[] sArrSysFont = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		
		// Recorremos el array con las fuentes disponibles
		for(String sFont : sArrSysFont){
			if(sFont.equals(sConsFont)){
				bCheckFont = true;
			}
		}
		
		if(bCheckFont){
			System.out.println("La fuente "+sConsFont+" esta instalada en el sistema.");
		}else{
			System.out.println("La fuente "+sConsFont+" NO esta instalada en el sistema.");
		}
	}

}
