package graficos;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaDibujo2 {
	
	public static void main(String[] args){
	
		MarcoConDibujo2 miMarco = new MarcoConDibujo2();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoConDibujo2 extends JFrame{
	
	public MarcoConDibujo2(){
		
		setTitle("Prueba de Dibujo");
		setSize(500,500);
		
		LaminaConDibujo2 miLamina = new LaminaConDibujo2();
		add(miLamina);

	}
	
}

class LaminaConDibujo2 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		
		Rectangle2D rectangulo = new Rectangle2D.Double(100,100,200,150);

		Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.draw(rectangulo);
		
	}
		
}