package graficos;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaTexto {

	public static void main(String[] args) {
		
		MarcoConTexto miMarco = new MarcoConTexto();
		miMarco.setVisible(true); // Tambien lo podriamos poner en el constructor del marco.
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Especificamos que al cerrar el marco se finalice el programa.
		
	}

}

class MarcoConTexto extends JFrame{
	
	public MarcoConTexto(){
		
		setTitle("Aprendiendo swing: Prueba Texto.");
		setSize(500,400);
		
		LaminaConTexto miLamina = new LaminaConTexto();
		add(miLamina);
		
	}
	
}

class LaminaConTexto extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		graphics.drawString("Estamos aprendiendo swing", 50, 50);
	}
	
}
