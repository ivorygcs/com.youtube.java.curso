package graficos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.SystemColor;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaColor {
	
	public static void main(String[] args){
		
		MarcoConColor miMarco = new MarcoConColor();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoConColor extends JFrame{
	
	public MarcoConColor(){
		
		setTitle("Aprendiendo swing: Marco con color");
		setSize(400,400);
		
		LaminaConColor miLamina = new LaminaConColor();
		//miLamina.setBackground(Color.DARK_GRAY); // Nos permite modificar el color de fondo de la lamina.
		miLamina.setBackground(SystemColor.window);
		add(miLamina);
		
	}
	
}

class LaminaConColor extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Rectangle2D rectangle2D = new Rectangle2D.Double(75,100,250,125);
		
		graphics2D.setPaint(Color.RED);
		graphics2D.fill(rectangle2D);
		
		Ellipse2D ellipse2D = new Ellipse2D.Double();
		ellipse2D.setFrame(rectangle2D);
		
		graphics2D.setPaint(new Color(109,172,59));
		//graphics2D.setPaint(new Color(109,172,59).brighter()); //-> brighter y darker nos permite controlar un poquito mas los colores.
		
		graphics2D.fill(ellipse2D);
		
	}
	
}