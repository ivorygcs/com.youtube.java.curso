package graficos;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.imageio.*;

public class PruebaImagen {

	public static void main(String[] args){
		
		MarcoConImagen miMarco = new MarcoConImagen();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}

class MarcoConImagen extends JFrame{
	
	public MarcoConImagen(){
		
		setTitle("Aprendiendo swing: Marco con imagen.");
		setSize(400,400);
		
		LaminaConImagen miLamina = new LaminaConImagen();
		add(miLamina);
		
	}

}

class LaminaConImagen extends JPanel{
	
	private Image imagen;
	private File miImagen = new File("src/graficos/bd.png");
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		
		try {
			imagen = ImageIO.read(miImagen);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}