package graficos;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaDibujo3 {

	public static void main(String[] args){
		
		MarcoConDibujo3 miMarco = new MarcoConDibujo3();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}

class MarcoConDibujo3 extends JFrame{
	
	public MarcoConDibujo3(){
		setTitle("Aprendiendo swing: Marco con dibujo 3.");
		setSize(500, 500);
		
		LaminaConDibujo3 miLamina = new LaminaConDibujo3();
		add(miLamina);
		
	}
	
}

class LaminaConDibujo3 extends JPanel{
	
	public void paintComponent(Graphics graphics){
	
		super.paintComponent(graphics);
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Rectangle2D rectangle2D = new Rectangle2D.Double(100,100,250,125);
		
		graphics2D.draw(rectangle2D);
		
		Ellipse2D elipse2D = new Ellipse2D.Double();
		elipse2D.setFrame(rectangle2D);
		
		graphics2D.draw(elipse2D);
		
		
	}
	
}