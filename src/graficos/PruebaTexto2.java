package graficos;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaTexto2 {
	
	public static void main(String[] args){
		
		MarcoConTexto2 miMarco = new MarcoConTexto2();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoConTexto2 extends JFrame{
	
	public MarcoConTexto2(){
		
		setTitle("Aprendiendo swing: Marco con texto.");
		setSize(400,500);
		
		LaminaConTexto2 miLamina = new LaminaConTexto2();
		miLamina.setForeground(Color.RED);
		add(miLamina);
		
	}
	
}

class LaminaConTexto2 extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		Graphics2D graphics2D = (Graphics2D) graphics;
		
		Font miFuente = new Font("Arial",Font.BOLD,26);
		graphics2D.setFont(miFuente);
		
		graphics2D.drawString("Guillem", 50, 50);
		
	}
	
}



