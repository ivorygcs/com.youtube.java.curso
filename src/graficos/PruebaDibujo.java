package graficos;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class PruebaDibujo {

	public static void main(String[] args) {
		
		MarcoConDibujo miMarco = new MarcoConDibujo();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}

class MarcoConDibujo extends JFrame{
	
	public MarcoConDibujo(){
	
		setTitle("Prueba de Dibujo");
		setSize(500,500);
		
		LaminaConFiguras miLamina = new LaminaConFiguras();
		add(miLamina);
		
	}
	
}

class LaminaConFiguras extends JPanel{
	
	public void paintComponent(Graphics graphics){
		
		super.paintComponent(graphics);
		graphics.drawRect(50, 50, 250, 250);
				
	}
	
}