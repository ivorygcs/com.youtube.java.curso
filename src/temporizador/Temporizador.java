package temporizador;

import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.Toolkit;

public class Temporizador {

	public static void main(String[] args) {
		
		Reloj reloj = new Reloj(3000,true);
		reloj.enMarcha();
		
		JOptionPane.showMessageDialog(null, "Pulsa aceptar para terminar");
		System.exit(0);
		
	}

}

class Reloj{
	
	// Atributos
	private int delay;
	private boolean sound;
	
	// Constructor
	public Reloj(int delay, boolean sound){
		this.delay = delay;
		this.sound = sound;
	}
	
	// Metodos
	public void enMarcha(){
		
		ActionListener aListener = new Hora();
		Timer miTemporizador = new Timer(delay,aListener);
		
		miTemporizador.start();
		
	}
	
	private class Hora implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			Date dHora = new Date();
			System.out.println(dHora);
			
			if(sound){
				Toolkit.getDefaultToolkit().beep();
			}
			
		}
		
	}
	
}
