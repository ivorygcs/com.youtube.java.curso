package temporizador;

import javax.swing.*;

public class PruebaTemporizador {

	public static void main(String[] args) {
	
		DameHora listener = new DameHora();
		Timer miTemporizador = new Timer(2500, listener);
		
		miTemporizador.start();
		
		JOptionPane.showMessageDialog(null, "Pulsa aceptar para detener");
		
		System.exit(0);
		
	}

}
